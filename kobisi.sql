-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 18 Ağu 2021, 16:48:49
-- Sunucu sürümü: 10.4.18-MariaDB
-- PHP Sürümü: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `kobisi`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `Companies`
--

CREATE TABLE `Companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `Companies`
--

INSERT INTO `Companies` (`id`, `name`, `lastname`, `company_name`, `email`, `password`, `site_url`, `token`, `created_at`, `updated_at`) VALUES
(1, 'Ransom', 'Kilback', 'Deckow, Kassulke and Boyle', 'pschowalter@roob.com', '$2y$10$s7290oJi1rOt2TspBh.JAuVqshwKP5cAdEvNrNt.hgDEcDeJTLxem', 'lesch.com', 'jVyJxcFOYdqplQheHcGwwC5vHI785tFl', '1980-07-05 03:00:58', '1989-03-31 05:01:20'),
(2, 'Camylle', 'Simonis', 'Mann Inc', 'michael.senger@yahoo.com', '$2y$10$ZpfzNeT/oTnCIBcZC5/qr.KPv72DPTHrKcXoYqYhaPZM.WG5RUe8y', 'stiedemann.com', '23svM4Wqos7dlQU9UfsaF9eFoEzO6GLx', '1982-05-19 02:57:59', '1973-09-06 21:01:49'),
(3, 'Vallie', 'Gottlieb', 'Koch LLC', 'bart89@will.com', '$2y$10$WEuGUgBh18X.uSiZi.tYQuzB2wx00dfbVKHcPIbIio6C/dxfsKXDC', 'batz.com', 'dE5EDkIZnAbtm5fzkiFitAWlVHqMgygV', '2007-01-08 19:00:01', '1980-07-09 03:43:55'),
(4, 'Ocie', 'Prosacco', 'Mills, Schinner and Mills', 'randall.hudson@hotmail.com', '$2y$10$QP/jzfdAcz0U71fy0ZavUOWPbEfa0ZEqtpFsCN2/Seksz9kIq0c0C', 'pfannerstill.com', 'PzkGRAVPGJ6OuxX4IK6mZj81jnZRxHZe', '2015-04-16 04:42:35', '2010-08-09 02:48:05'),
(5, 'Eveline', 'Harris', 'Padberg-Mayer', 'elroy.lebsack@hotmail.com', '$2y$10$tZGrZbJ.lCuaWLlWsdynz.0MUafP0Pp2Ohjcsvwo1ii/prh7JDnNe', 'lang.org', 'tB5ONr0f6kmFkdNTyt1cdKcDs2Bx1iTk', '2004-02-03 01:53:41', '1977-09-16 00:45:12'),
(6, 'Woodrow', 'Funk', 'Koss Ltd', 'troy.damore@gmail.com', '$2y$10$G1L7LG2lzEAlqcwNW7qYZeZHjH91SU70nIjMMopT7vsXcrZOsgu/u', 'huels.net', 'LHp8ZUyV4aXY4uBhYFgQD9KZQ1tsMHiG', '2021-04-28 11:53:07', '1989-03-05 12:52:17'),
(7, 'Magnus', 'Herzog', 'Eichmann, Littel and Cruickshank', 'lester77@howe.com', '$2y$10$mJ9UcJMp5q.3jasn8Ogm8uL1O5uXdnYIGp4ZXIkyveKVMIY8Zfa9y', 'muller.info', 'WkPgiS0yteM8YrQD7iVw0Cd9nQKhI03r', '2009-08-10 19:33:07', '1988-08-26 15:16:17'),
(8, 'Will', 'Rippin', 'Conroy Group', 'cummerata.olen@bailey.info', '$2y$10$wY0o9aoDxtmthaqoWDW1U.QxCwRJFHwvbqcXvkhxrtAH6KZZBkU1m', 'mcclure.info', 'bCxiPZnEFwAAsMWrmduTywP3zWW5THvY', '2017-02-12 19:02:13', '1977-06-30 07:54:26'),
(9, 'Loraine', 'Hickle', 'Maggio, Bruen and Hane', 'eleannon@turcotte.com', '$2y$10$osb0gwBSJeJ5XJGty9Eaquq1GD8EeupyExVs7Tdy7.YCuf1pMjYU6', 'conn.com', 'rN76y4lSddRKsl3AtEfANfnLpMCiSzgM', '2005-03-19 11:06:04', '2016-03-07 01:13:10'),
(10, 'Everette', 'O\'Conner', 'Greenholt, Denesik and Waelchi', 'fupton@gmail.com', '$2y$10$QVgL6eViuPWJvD3PVMh68OeWPfRXm2TTIwbLgPE.2BrZ7rgdXA1/C', 'kautzer.biz', 'wmyU2XDbAFzHPUhQiNbDqTi91hlK9DlA', '1974-10-15 20:50:03', '2011-05-10 23:31:55'),
(11, 'Garrison', 'Kilback', 'Sipes Group', 'pete.rempel@yahoo.com', '$2y$10$CkAVa.0FkT1P0fqu0xF3ruYE6PJcGdA7K.FkoVO6qMEZYNBpk2Ele', 'lebsack.org', 'cRfx5WQjLb9Qh2HzCR47mQrGCnifhI7W', '2006-06-11 04:41:36', '2000-09-01 19:16:40'),
(12, 'Nash', 'Olson', 'Mayer-Herman', 'collins.telly@hotmail.com', '$2y$10$G7dV.Zkq6EFGY28/MN64.uZhrtR89RrtB9BOti/M2/EGeRojPWrqG', 'hamill.com', 'bdVt1tPCbe0Zyt5d5eB6TsyRadsNHgPt', '1974-03-23 01:07:46', '1972-08-23 21:09:12'),
(13, 'Virginia', 'O\'Keefe', 'Weber, Watsica and Rosenbaum', 'rolando.wisoky@osinski.com', '$2y$10$/j4Io7oGTKimeLh8TyxVteK5XxzJcSnrKQJ4GnSUkQ8vePVA/hYnS', 'boyer.net', '8zp27JOkCUTMc9fkYVylNc4fhupvjCj8', '1976-06-19 20:45:48', '2000-04-23 16:21:34'),
(14, 'Shyann', 'Lakin', 'Zulauf, Koch and Schultz', 'millie.nikolaus@hotmail.com', '$2y$10$stLQUDugqxql0HDVgHEKNe6krSyOKeMFeT.ms5pKdozMnfC4m0AG2', 'kautzer.com', 'mQeiMumViRvXIKgFbe7rix50eYKioV8Q', '1988-11-21 23:56:45', '2017-12-11 09:38:06'),
(15, 'Gertrude', 'Howe', 'Kessler-Labadie', 'eino63@hotmail.com', '$2y$10$9zLrdc5hOlo8FXqiLE7WU.VEKVXANdwF9q3JEIS8pmksj8luz0L1K', 'miller.com', 'hTvLjg2v1OlSdOiRq5jIMWpt9orQ3pLF', '1989-01-30 11:55:27', '1971-12-20 17:16:57'),
(16, 'Corbin', 'Hodkiewicz', 'Labadie-Maggio', 'jaylen.runolfsdottir@yahoo.com', '$2y$10$ULAjLgP65kauQbwOsWNQwuCAnfs.YaefkdtFN7QHnmMMXipscgECC', 'feil.biz', 'zcfXTSZoCx6Y2H8kyxtZKkXjJyUh5jWq', '2020-04-10 23:12:14', '2003-03-23 07:32:26'),
(17, 'Immanuel', 'Bechtelar', 'Cartwright-Jerde', 'fratke@lowe.com', '$2y$10$LNyDA.F26vEx1LNL0uGQRe9i5WkPF.zz6SBYf/bQvYYv9kKV/8Trq', 'hill.com', 'nt25dMk09Xrwnj6N0Qzvjv1ccElfDIsF', '1987-04-28 20:44:14', '1972-04-04 17:20:02'),
(18, 'Ignacio', 'Ratke', 'Windler-Bode', 'eryn.stehr@hotmail.com', '$2y$10$3PcoN/yOJrZKQ4nNxngTZ.lZZhd1WqZ6fBY5MSiNcv6/zqFLGhP6i', 'gutmann.com', '2YobiFvVlG1d5s6dsXukSeLrJTfkjLy3', '1972-08-15 18:16:00', '1997-10-14 22:34:41'),
(19, 'Jocelyn', 'Wisoky', 'Brakus PLC', 'demario.hackett@yahoo.com', '$2y$10$orNoaiMPaCYPBT.FN/Om1u6Z4yjuz2Noh89Yq8wyxOunT0bNn.6X6', 'bosco.com', 'wDRYoR1emJdCrZPwQA66DdHy9j1NeqGF', '2009-03-03 18:56:37', '1994-07-28 19:43:07'),
(20, 'Clement', 'Donnelly', 'Medhurst-Nicolas', 'rau.elta@lubowitz.biz', '$2y$10$gWIE6zcP/DvoFgOQT6nfKeJHRN9DYpR8RoX963hNKAYXukNRqJLE2', 'torp.com', '89ktgNYdnxlG5Avpf72mGkieGhvm4CTd', '2006-07-07 19:19:57', '2014-08-28 08:25:46'),
(21, 'Beryl', 'Quigley', 'Labadie Inc', 'ylittel@corwin.com', '$2y$10$Xz.8oJ5lgW.gN919ITAX6uGl73ulYr31YH5KaSCvoDs/sUDztDKni', 'koepp.org', '9QpXZmwEQXbwGUy9E0zOupBAyfwnf4fw', '1989-12-07 12:29:43', '2001-08-05 17:27:53'),
(22, 'Hermina', 'Kreiger', 'Gleason, Mann and Boyle', 'narciso.jones@bauch.com', '$2y$10$m0njJNErr33N8faKl.KAKO9mNIFUUw1iyAjl.mn/rldE.leq33ycy', 'klocko.org', 'NBVGtKhJL4NiZgAe2Dta0hcwXUcDQycM', '1975-11-03 21:30:34', '1974-11-04 18:37:06'),
(23, 'Cornelius', 'Hegmann', 'Hermiston, Kiehn and Denesik', 'antonette.mayer@bernhard.com', '$2y$10$OUVhraKkaIy6Dp5dG7mmo.xzA4nfE9Pmx.7MM0we3fhQ4oISWaHIW', 'hauck.biz', 'xVSjN2KEYtyDNg7iVYcukWXFeOgqoIpf', '1985-12-07 07:13:46', '2013-10-12 19:14:55'),
(24, 'Coy', 'Ward', 'Johnston-Corkery', 'oral.lebsack@reilly.com', '$2y$10$11gc/vhHRUZg1fET6BvQyuHdYKbZxJyEgniX2WqzLWhiRMZEYE8wy', 'sanford.com', 'Q5DlfLzQmkE4gTapokHxFpsWYv3RIXlg', '1971-07-12 01:02:44', '2005-09-19 03:59:00'),
(25, 'Yasmine', 'Kemmer', 'Carroll-Schuster', 'mandy.lakin@yahoo.com', '$2y$10$rBEPkC4ntBab1X5sKM7mj.fQq9ccK1loeSRXNzfNCKGdxP25nzzla', 'emard.com', 'WkC3JO87Yu7C0NTbuys3K8qQvlYe8F7L', '1981-12-12 21:27:42', '1981-08-20 22:46:21'),
(26, 'Mustafa', 'Sporer', 'Nicolas-Bergnaum', 'conn.domingo@gmail.com', '$2y$10$Xkv6UxjETIDCR1D5RO.MS.H0ksBjRGWqXA7OduSeeiARGBtJ8a0XG', 'lakin.com', 'gOCDcMUtPxFKEVYalDyxi98isLINLk0o', '1973-09-27 08:37:15', '2001-01-15 14:38:19'),
(27, 'Kattie', 'Considine', 'Ryan-McCullough', 'garnett.stroman@hotmail.com', '$2y$10$.N6vEN7ZV1QXpXI37bAxj.W2fnD7/5wnwVlbMgHkOCOSX/PD0zD/m', 'barton.com', '6i550FO9ysGwdgqwA4wRWtaau8oMDzmG', '1971-07-15 21:29:54', '1981-12-09 20:24:50'),
(28, 'General', 'Romaguera', 'West-Rath', 'smoen@nikolaus.com', '$2y$10$pzcVGLDKdfL1XEnxHYoEsetd432RuKr/.3AybhR7KYz6oKy.mG5f6', 'hettinger.com', 'hfgjLpLe1kKuaFVZXxghWq6o9WiiQ2qj', '2019-01-05 06:24:49', '2017-05-23 22:50:34'),
(29, 'Spencer', 'Aufderhar', 'Welch Inc', 'frederic23@wisozk.com', '$2y$10$fCc1pkqJeIsettuNOqZ1Ee.cBTLP3UPFHICCQ0YWRiicg.Z9SVz8e', 'emard.com', 'pH3SBir3r6SIQMxBKXRNELptzTXXYUSl', '2007-06-19 12:57:49', '1993-04-27 22:26:28'),
(30, 'Chris', 'Smitham', 'Kertzmann Inc', 'leuschke.maxine@hotmail.com', '$2y$10$7UTU1FdRP.NhYZO026VRhuPvg.GNgUi7rfuvSZF0PZ7OhNm01eJB.', 'maggio.info', 'iRXxKpoKBAU0FrsHuhI80L5qJRPb2lmN', '1981-01-10 17:38:36', '1973-08-12 02:51:00'),
(31, 'Theodora', 'Ruecker', 'Hegmann Group', 'umohr@hotmail.com', '$2y$10$.XtNPjda0sbvSczY80LTXuqi8u9Fbi0MhFjRr9tHxiiCmvRbY9y56', 'lebsack.biz', 'h6bINSmwycB19NzaYSYfLK9PTnbhKhEB', '1970-05-28 11:01:06', '1972-11-05 15:38:12'),
(32, 'Frances', 'Schaden', 'Waelchi-Ankunding', 'ephraim.dibbert@oconner.org', '$2y$10$18SDn5LkkgtAQm50Oz9mKOc5zFpZ/pZzuQJXhBumdMQh5OQD7xpS.', 'ohara.com', 'c8v1IyEEFt9HJzaQoGc6aKgUAThVHOCl', '1998-06-14 04:22:09', '1970-06-12 13:35:44'),
(33, 'Kevin', 'Strosin', 'Jaskolski, Kub and Corwin', 'cameron.little@gmail.com', '$2y$10$19dYwKL5Y18JqpSrejafYeVJGzUdJjnTDUVPU4QhYrOHQG4ZRtpgi', 'stoltenberg.org', '49qsU1eTA3UZKLRaX2wj7GU3R833cXqq', '1999-02-19 12:51:10', '2006-07-31 17:32:55'),
(34, 'Maribel', 'Becker', 'Sporer Ltd', 'kjacobs@gmail.com', '$2y$10$II6OhoqmNxjlMNLLY/rwOOzsau71kd73qTYVoxxBTMEmWSb/wpZF2', 'cormier.com', 'xMGnHtaXe0djGOFDPELDeilepDpQeQoM', '1997-10-29 07:29:10', '1980-11-17 10:13:54'),
(35, 'Columbus', 'Johns', 'Johns Ltd', 'vicenta11@torp.com', '$2y$10$yeSifbiBiOQrz4wGt4vV/edfehpFIbKYniXMvMhstsfMQ2fQOw6tu', 'shanahan.com', 'KEb3jCpIEqcr0FFLsJF6WWI5qZC3PdsN', '1998-03-13 04:52:24', '2000-09-12 21:46:26'),
(36, 'Malcolm', 'Fay', 'Rempel Ltd', 'mschumm@gmail.com', '$2y$10$aWlMs260d02kC0kxYQLzhujy/1wDAAw5yGn/zEbcoEzp3HDnca16W', 'murray.com', '2nRt56g7ajH4Cbf5eonrjfLl0O65EoG0', '2007-06-10 20:46:12', '1998-01-14 18:57:30'),
(37, 'Genevieve', 'Hamill', 'Treutel, Feest and Connelly', 'johns.gail@gmail.com', '$2y$10$lUlNm/UZjVbE11ggqFOFVuifFDFY8YvXt4EB22hSObhKEgZiqm5wm', 'stehr.com', 'c4Ks8FYUmUXGX6wbnQZxjGdOQRoGNpxh', '2002-05-11 22:23:13', '2006-08-20 12:54:46'),
(38, 'Dashawn', 'Effertz', 'Bartoletti-Hill', 'joan51@mayert.com', '$2y$10$7uK4xV7kob4M5w/lMs6st.l61m1XZq9P1wauuiTrhIz5ZHSiErQ72', 'shields.net', 'DUCEsiGOQtTx3rnPI7n9X6BzBvIgSWfK', '2008-03-01 09:58:28', '2014-05-08 19:31:19'),
(39, 'Stanley', 'Muller', 'Kessler, Hilpert and Shields', 'marlene40@ferry.biz', '$2y$10$g9xnxDUBVEd5qt5KIMKiqObU5zCWbSeAtLyKjKGh62xdndC0v5Jhm', 'dare.com', 'L7AZuQ6CW0oS4QdFHgRz7e4jUsaU15jD', '2005-06-26 16:23:50', '1999-10-09 19:21:34'),
(40, 'Pierre', 'Schaefer', 'Bradtke-Dach', 'pwyman@gmail.com', '$2y$10$H73ucY/7UEc7dPuUhg6XFeqXQHTyUXfcrEgc.HGuhAvA8n6uPiHfe', 'mann.biz', 'wZoyFXINRx0e1ZmDOzmMIZ52zn4tKyzi', '2011-07-30 03:56:17', '2010-01-06 19:49:24'),
(41, 'Angelo', 'Koelpin', 'Reynolds Ltd', 'boyle.amya@stiedemann.com', '$2y$10$NwjO5g9Sqcgpk2qAJerzWeVsR4R4UGyaWOa110HSx5J1zwKwkbU32', 'hand.com', 'ZTiSSEjRdAPReTw1BspmTshDdoCjJ3sU', '1984-06-05 06:54:19', '1982-08-26 21:57:11'),
(42, 'Ephraim', 'Ondricka', 'Casper, Kling and Rau', 'dondricka@hotmail.com', '$2y$10$OfrI7Ly4AJs9h2Cvk.PV7uo88Q906E.4m18hQWclyPcOuGSlXD9XO', 'mckenzie.info', 'sfMDxZxjIhzUtUD6U8Blvjao0QPzM21F', '1975-03-06 08:47:08', '1989-09-26 20:27:06'),
(43, 'Lelia', 'Wuckert', 'Buckridge Inc', 'klebsack@lehner.biz', '$2y$10$yeIW.wXw.M8pPP6Iur5nN.PWyCg6QFmrUhcfFEIMm85yvtS3jrdxm', 'marvin.net', 'CsmsjW0wFC21RTuYHahGABOrG5MwEmve', '1991-02-17 08:03:16', '1998-08-31 21:07:00'),
(44, 'Kaela', 'Ankunding', 'Brakus, Abernathy and Altenwerth', 'leonor25@lebsack.com', '$2y$10$XjHy/1ZzjJOhH7HP.yH7a.w2ztzT1rahkZW1P17TukFd/FQrWXcrK', 'homenick.com', 'w0bFaMnEeKP4R5nDUzDTP2ityW21GXW0', '1987-11-11 02:00:59', '1974-05-01 16:24:28'),
(45, 'Katelynn', 'Gislason', 'Franecki, Cartwright and Luettgen', 'zjast@howell.com', '$2y$10$hm0CFSVQps60eCIFX7b/6OCliJ0Egr.QJ7YhAd.s1lnSW/tG0CCOi', 'rowe.com', 'iOyy8VT0815VVUWVpmrRyBMJUCyGaeur', '1995-09-09 03:05:05', '1987-04-15 19:19:19'),
(46, 'Daphne', 'Bahringer', 'Bashirian, Schamberger and Windler', 'ferry.donny@hotmail.com', '$2y$10$wWDC51lVvBXWqRQnveXLcePjnachjzal5dKT/txa71T4rJ75xI45u', 'hickle.com', '4Rqfa3X49HNeDhTv11CBAJb6HDF6aTSI', '1974-11-27 00:03:09', '1979-09-26 19:00:03'),
(47, 'Maybell', 'Huels', 'Orn-Wiza', 'jarrett28@kuphal.com', '$2y$10$LqDz4rPcOVfrB.9D.5A72uyZQdpZ8gyVKKsQePqFaT.83YMhl2MgK', 'schneider.info', 'yCYn7ODzuSQvY4UqxInoeb6KQBg8kMW9', '1996-08-01 22:58:11', '1978-08-14 16:15:25'),
(48, 'Magali', 'Pacocha', 'Weber, Brown and Krajcik', 'thad71@jakubowski.com', '$2y$10$Iut.ir/Y8xButiND8OdFV.wyXVgV9MmqRXdjrcQ337auxC.blhqm2', 'hammes.org', 'sAwH4HQZgWtrjPIQe2nlDyTJ3NpxXac6', '1973-10-18 02:52:08', '2018-08-29 09:27:15'),
(49, 'Westley', 'Lemke', 'Runolfsson Group', 'armstrong.modesto@yahoo.com', '$2y$10$5aBADRI6zcDA5Er.VJMws.WPMtX86gkIHipfYf2FavvRbggpanbGW', 'reichert.com', 'psOsBbeiMBQU0c6GqiHdLDNqEOZ1ut7r', '2010-01-28 19:58:30', '2002-11-10 12:41:10'),
(50, 'Manuela', 'Konopelski', 'Spencer, Wiegand and Gaylord', 'ioconnell@abbott.com', '$2y$10$Uobxf11Q0jNG8LSDLFLsuOiCZJgGExJCCh/akoXOR8yD3aL1vOjAK', 'purdy.biz', 'CILOzOt6kceR9jKBPVyzONZPP0BsLmfy', '2012-12-29 08:38:29', '1974-12-07 06:49:27'),
(51, 'Carlee', 'Rutherford', 'Ebert-Marks', 'jlangosh@gmail.com', '$2y$10$vWD19nZglRmrA2/R2aCptetPqWCGWWdNIiKWWlhnb70gom/vmd.KC', 'cassin.com', 'vfM5mZmVjdLNf3W2soWPzdbjnKOJjfF4', '1977-11-16 07:03:33', '1990-07-18 04:37:55'),
(52, 'Colten', 'Heidenreich', 'McDermott, Miller and Senger', 'margarett77@gmail.com', '$2y$10$6vkW/V8yRm/NO5Rnz3iqXeSsf1V9QHD.hs9jpuv9kN8Iz4MgkHNrq', 'crooks.com', '9HUkMJJMhcoNk6sMasb9i0kyiIJ6MpgA', '1972-09-09 04:46:10', '1987-02-27 03:02:15'),
(53, 'Christelle', 'Auer', 'Ebert, Kunze and Padberg', 'dicki.nyasia@shields.net', '$2y$10$d6aiCxK96xKpn0a.T1N8geDHqZaJ71bSzB6A333JcYtf6iNMOaS2G', 'walsh.com', 'RCKeUmYpmul9n5f1Jcv01lR561ijoAxM', '1993-12-03 18:01:12', '1992-01-13 00:50:19'),
(54, 'Marguerite', 'Stokes', 'Hettinger, Pacocha and Daniel', 'jennings28@daniel.com', '$2y$10$8ew5yr/1Aj4k7gn8POCjh.avytJwMpiVMRjxzlc0WMX3wgE0XsBMS', 'schowalter.com', 'ofbZ2ohPx9FkvBSy1KkuDN2ael6ri8PP', '2008-10-06 10:05:19', '1997-06-16 14:38:50'),
(55, 'Elda', 'Schiller', 'Gusikowski-DuBuque', 'afeil@mraz.com', '$2y$10$6RoHbTjfEzA/iY.3gvJh7.P0AZ0Sr9igWhJp5DtA19vFp2tK7Av4e', 'reinger.info', '6ABjDWi2kT9IK1Ozgqd3YQ0n8Nu00In3', '1982-11-03 01:11:35', '1988-10-29 04:28:18'),
(56, 'Rosella', 'Hintz', 'Goodwin, Collins and Tromp', 'cdavis@gmail.com', '$2y$10$OLxG2At/nI12mWKT8w2epe/IgvCb8A0V2whp1XFwHcw4BJ3SMglOm', 'ledner.org', 'vDY0nEzlhXX9efPuX0tssofJOIU5M0Fi', '2010-10-28 16:43:43', '2007-09-16 05:39:22'),
(57, 'Ova', 'Prohaska', 'Wuckert, Moore and Veum', 'lwillms@hotmail.com', '$2y$10$ihragv8cugnJouYnLu7as.nsb7zTFk4CIK1X7cxh6tZ/CQm5C0ZDW', 'hamill.com', 'F8veaXtNxOhbBpQVeGBsZklvIywqYtda', '1984-02-24 22:40:17', '2008-04-10 21:41:46'),
(58, 'Nathaniel', 'Wyman', 'Smith, Moen and Kirlin', 'franecki.skylar@gmail.com', '$2y$10$v4cSPU8mjrPINx3L9x65lOgMCUXWT84S9qvHC9e9xbcagj.J1MyMm', 'kris.org', 'o37lZl5cnjKqsdjjz5gZKoPpH3nQQsYX', '1999-07-23 00:45:21', '1971-04-04 05:42:51'),
(59, 'Herminio', 'Nienow', 'Barton-Windler', 'alvina.deckow@raynor.com', '$2y$10$mE6jWX1Zri/SnL1K6MjX/etDDQAnH3NZnGILm66yesJZGsdFnLfXS', 'jones.com', 'UCN3d4xY4V9rQAWUXh5dAkXWga3srGbH', '1992-10-09 05:09:16', '2012-03-16 11:31:53'),
(60, 'Dock', 'Balistreri', 'Lebsack and Sons', 'glenda01@hotmail.com', '$2y$10$f9Acu0TArVw9wFClJnJ1H.enxKDsgPELfSRGqJE.oQLSvCLx79PxG', 'walter.com', 'nxm899buat3Oh8GKUrbmDgbtIMI9zOrS', '1979-05-30 02:38:14', '2015-09-11 03:13:13'),
(61, 'Rubie', 'Wuckert', 'Kreiger LLC', 'emmerich.roderick@runolfsdottir.com', '$2y$10$odVNUnEgCjjZ2LjaIfsuYeE6IHQ3qdKxYIuYkHYDknRPjBA.HlWfy', 'barrows.com', 'oNPt2Q1xYAJLqJQiEJCRrNI0WtaLczKH', '1985-04-07 08:50:42', '1987-12-31 18:28:19'),
(62, 'Melba', 'Sawayn', 'Beer-Lang', 'wisoky.hank@hotmail.com', '$2y$10$boF.DIFkqFqXHwlUeFM.muUUhCsHdWb.LLQe47G9N31pTsdXZjVDy', 'hettinger.com', 'g7CwxHKjTx8Dy8ubYz4g1Yy5zkVYcjI5', '2008-07-18 21:49:49', '1979-03-18 04:40:27'),
(63, 'Austen', 'Corkery', 'Kuhlman-Langosh', 'briana17@cummerata.com', '$2y$10$vW.yNfDvmbzxgDdrxA4M6uOktphdkbtc1yw5bWLMecJ1ifzHnwuRm', 'lehner.org', 'J609E22niHpltlyDwZh3fXuXZfQX4Ies', '1997-09-28 05:11:20', '1978-09-04 14:56:49'),
(64, 'Isaiah', 'Barrows', 'Dietrich, Skiles and Padberg', 'schuppe.nellie@yahoo.com', '$2y$10$YrgKwWhemC4kJh40fsSvjeRA001nHHy7ZjP8ITytq6Vg5AP1QSMWy', 'leuschke.com', '1b3C9dxSNoMvwChkpsD0HbhHJTC1FHDN', '2003-11-11 08:58:12', '2010-02-19 10:08:36'),
(65, 'Haskell', 'Trantow', 'Lowe Inc', 'fahey.renee@watsica.net', '$2y$10$nNTPAnBMDdh7j2LiIN7UI.SKn/51uzXgu7Um0SKqEIDZhX/ywP48O', 'jakubowski.biz', 'Kk1moS3FmLlSbYqeJ8Qia47E4etzwRwq', '1978-11-30 21:26:09', '1980-04-02 16:13:51'),
(66, 'Colby', 'Carter', 'Fisher, Bechtelar and Tillman', 'tommie31@wolf.biz', '$2y$10$Q8e5X7EvsBMXorVxRvNUX.JubYdKvxWWyFqJKXtIBg52JrQRugnuG', 'mayert.com', 'wLhewCiS9BSCtVZD6J0Ob8SlqQCIs9Za', '2017-10-26 19:18:37', '2010-07-14 00:14:10'),
(67, 'Carey', 'Barrows', 'Mertz, Wyman and Bins', 'koelpin.mekhi@nikolaus.net', '$2y$10$GjNbs7vQjXdi1k6p4i/b4eo76SXQ4xAvHXYsffu61HJy3R/nSORIy', 'mayert.com', 'TRcCM55UB5IWqqrm50LHChQxPESqRDfc', '2005-10-01 04:05:32', '2020-09-30 07:39:44'),
(68, 'Herminia', 'Miller', 'Padberg Group', 'lwisoky@orn.com', '$2y$10$PG6RYRGSzKSKCYpWKIJdauuh2iyovXHMW8f7VyOQFhjbFGT4sfueu', 'osinski.info', 'Ju44mr2TmIO3JQNit3sdYGc1zWqVyjAH', '1987-01-16 07:37:30', '1994-10-16 19:42:30'),
(69, 'Camden', 'West', 'Huels Inc', 'wisoky.evangeline@gmail.com', '$2y$10$41x307l6loCCm.HPT1WsBuzZemh4ciaz8Sg7.p07yG/mrnLwxUcfW', 'bode.biz', 'cNR4aWYLTCAykHrPYOiPwS5prmrtNUYs', '2001-05-14 10:56:14', '1985-03-14 10:06:30'),
(70, 'Bridie', 'Keeling', 'Stiedemann, Emmerich and Stracke', 'qhowe@pacocha.com', '$2y$10$.2W5uUvbbG8M0eMDTKH/TeoDc9Jl735O5G64w6Po.GVuTSRedhV5W', 'terry.com', 'ZfL1qp9L5bvPMrGWFuiaspovNHoPhdWA', '1976-07-24 15:09:27', '2000-09-16 08:17:58'),
(71, 'Madaline', 'Rodriguez', 'Tillman, Kiehn and Wolff', 'cleveland.armstrong@zemlak.com', '$2y$10$7CpcFEpjI7ipv4AvS3BL5uT8LVws6hjHL4AyRtdaedj3DKexRN/5m', 'mcclure.com', 'OLERM5fcOOCQ2utzwbv8aRiPmXbKuoKM', '2003-08-15 05:51:13', '2005-09-30 17:36:46'),
(72, 'Tania', 'Rohan', 'Mueller-Koch', 'iwatsica@ruecker.com', '$2y$10$pxrm6rB1Hrpt6ruQCQwdSelk0q5SU990coDPN0FVb4R65bL1jftG6', 'hammes.info', '9HUHBugj4WvhpMrAALUUFhiLO5xlcDIt', '1996-07-16 09:59:32', '1975-05-21 17:40:54'),
(73, 'Tremayne', 'Casper', 'Carter PLC', 'juston.beatty@hotmail.com', '$2y$10$N/Bgsn8q56eV6DQAu19ihez3HcbNM5PjnXhanpD7W0EwiCWa59c9e', 'balistreri.com', 'iqPIohdX5iFQu3vcVXjacfUyqVxDBpy4', '2011-07-08 01:06:11', '1986-10-17 12:10:31'),
(74, 'Elyse', 'Keebler', 'Farrell-Thompson', 'shawna15@schneider.org', '$2y$10$.73JkZfx7xCB2ZxUPIcCauGREamva85ebdmmfG0Qj3d9Uya/pFYm.', 'predovic.biz', 'Vqg5GIubnlanfxdZdOzhEK6q6woFbhcI', '2018-12-23 04:10:32', '1973-04-16 16:40:53'),
(75, 'Bo', 'Ortiz', 'Satterfield-Rath', 'vicente52@gmail.com', '$2y$10$ktRVCo2Row2BwlepAIUMk.2oMoCyvLfbn8TSl9Rsh2O.G79og8voW', 'rau.info', 'gPubz5VUP9VCmAq8Eftq8dt9TEKEA67r', '2003-09-21 18:24:07', '2002-04-10 11:48:12'),
(76, 'Sim', 'Bahringer', 'Runolfsson-Hintz', 'mcdermott.dereck@boehm.com', '$2y$10$H.anfKeH.VfSffHQCmoWaO77885.yD/vHVtJo6aoRg1h1k/6xL8bK', 'schuppe.com', 'xWVfhxfIkrfElrjbDmFKeQbOi4O3nz9L', '1981-03-27 01:50:14', '2000-06-15 14:44:46'),
(77, 'Marlin', 'Bechtelar', 'Lynch LLC', 'jfranecki@west.biz', '$2y$10$1mmi4VYbk931rYCHRChJxeP7bQGwmF77qA6wgTHWluYqtVs9bzIIO', 'weber.com', 'G7VAfHhcKZglKFQH6ZG3a6ok9eHMSg3w', '1973-11-23 12:31:10', '2006-01-20 20:45:48'),
(78, 'Luciano', 'Considine', 'Hayes-Weissnat', 'buford.rempel@hotmail.com', '$2y$10$sIx8B7S3WKfJdRi1pudlHOAoeVXVw5NFnsGKO1lPq56I3qaYHt1cO', 'gerlach.com', 'PwlL2gNnsNhJhJW1QNd9fYZAtyL6miNf', '1991-10-02 00:59:00', '1981-08-03 09:22:57'),
(79, 'Trenton', 'Rempel', 'Auer-Spencer', 'zframi@gmail.com', '$2y$10$xrG7DQrzenLIjpxt.XMGS.23/Gap7dH.iG68ifosfNE5JG2dL6H4m', 'damore.org', 'Ae33OTCpdjPIza9xeLRkfBx6IA7dvqde', '2008-03-16 15:37:37', '1970-10-26 20:06:26'),
(80, 'Claude', 'Muller', 'Dibbert Ltd', 'stracke.shana@yahoo.com', '$2y$10$NiVQPOnWAXKSf2pooF41OesL.2jKwwFKfXAWUJQIYIf36qSvwQ5vO', 'wehner.com', 't0dFmyg4trgyjvbNJIIM02EwjLIE6grD', '1992-05-23 20:54:26', '1981-12-27 16:11:32'),
(81, 'Cristal', 'Borer', 'Franecki, Botsford and Lockman', 'emil71@swift.info', '$2y$10$i/tb0EMN98clj6TForvv2.5oQ8OcLL5pVx4DqhRww2yCGVETsKhDG', 'roberts.com', 'VUPkqhnx6rM6D5Zwfj4CR6QWKxW6fd2j', '1986-10-29 19:57:05', '1981-08-12 02:22:21'),
(82, 'Grover', 'Mills', 'Lockman, Dietrich and Block', 'makenzie.hyatt@littel.com', '$2y$10$zn.8DeAdG3BehZ3ghUMFN.hJWWNtDBczXaFJ7yT9P4MQkTs43LpuK', 'swaniawski.info', 'S6yOq9AaqHX5AIXQXsnqWz3M9XUcIXXk', '2007-01-05 06:17:07', '1985-03-26 14:08:14'),
(83, 'Annabell', 'Crooks', 'Spencer PLC', 'ardith44@gmail.com', '$2y$10$KJAHJ4pdysD3nOxAQivFsuiPmTHVhJvjXoFRPq43YgvSEvUCKlO5y', 'kiehn.net', 'qoRMczXRSEuvP8Hsv3cw2STBqPWG9Ojj', '1985-05-25 08:52:49', '2019-10-23 21:50:46'),
(84, 'Brisa', 'Beatty', 'Witting Inc', 'hunter.funk@gmail.com', '$2y$10$HHut3RRWCS304bTEZiV./eHHTpMAxjNK0cxoBSf/eEr9OhlhHbLXW', 'bashirian.com', 'Lmd0n5HUIARJfhienlRQ5CBB1Y13vtkG', '1988-03-25 22:54:29', '2017-11-16 05:03:35'),
(85, 'August', 'Schmitt', 'Labadie, Conroy and Kuhn', 'ljones@hotmail.com', '$2y$10$q4d8t4pSiUTI7FbLxuowIO.r361EgsvyOnL0B.4p2f/.h9xkdHLTi', 'lemke.info', '4WSa6olZplbpnDKvEF7mzhxdZAtyhaVv', '1974-02-21 11:49:39', '1988-07-02 11:37:53'),
(86, 'Leslie', 'Hagenes', 'Boyer, Renner and Brekke', 'benedict94@hotmail.com', '$2y$10$bHQntmoH9rurPkAzbLdWiOxxSi3cpc9cyhrXUZV7BJ2y/72R7jWz2', 'schmeler.biz', 'sT7K56Q8kfentyJg2x9Yb8hvkZN6E1sf', '2018-03-08 05:25:01', '1979-09-29 04:38:42'),
(87, 'Jerel', 'Auer', 'Ritchie Ltd', 'dtremblay@langworth.org', '$2y$10$qqdLs4dRtFHp0l0dQYzl4O/iU76TpOgzdW.QMqMnNsb0cZU28xyaO', 'mccullough.com', 'WGRdYJi20EbIoX0JS0Ex3cjWsXqRGRAv', '1973-05-17 21:40:52', '2010-12-05 14:31:01'),
(88, 'Xander', 'Aufderhar', 'Walsh-D\'Amore', 'hoppe.millie@beier.com', '$2y$10$lwHAvIiWfCs/Jp/ayw0FcOfBZCPsYIvfZ98nAlP/Xid1j7cYa0p7a', 'bauch.org', 'kpWJEezlnAnV3OUM8BbS81bj9fzsqhwf', '1973-03-05 07:44:35', '2012-02-18 04:06:04'),
(89, 'Macie', 'Crona', 'Wilkinson LLC', 'ed.kertzmann@yahoo.com', '$2y$10$avg9JH6DThGfJKURIIRN4O6glYvIjnr.TqpB8tW/1fYPVWociSL82', 'feil.com', 'pEkWvP3d8e6z99Y0tp7v07HW1fPsYXV0', '2008-12-31 14:16:20', '2006-04-03 09:24:07'),
(90, 'Mary', 'Heathcote', 'Walker, Schowalter and Beer', 'ometz@barton.com', '$2y$10$Hl3r43pdd0WKbWMEAeBOnemDjJzEW057YmrtmnxETywmF6Rxwjf7G', 'cartwright.com', 'rLnja2Fh6kLqn7q3sT38fFGCz8o8hv5A', '1994-02-26 23:27:18', '2017-09-26 14:47:25'),
(91, 'Virgil', 'Waelchi', 'Thiel, Kshlerin and Mann', 'collins.fatima@mraz.com', '$2y$10$lElWC7nNkycptnJoihAWlupWZFNPNkB3bnEtndkM0mkRFHeQ/KXLe', 'dubuque.com', 'jOspbUXXHeusWeiWhp1VbtiaHtsYBE6C', '1978-03-05 21:41:36', '1975-01-30 06:35:36'),
(92, 'Jesse', 'Brakus', 'Bruen, Rice and Kuhn', 'aspinka@bartoletti.net', '$2y$10$c/s7pt9T/hWezRycLE3KzuwRNvCKS6kXLZIlTZvL5HAzE2IsqETUy', 'blick.com', '1F8IN6svUhxCjKrXeleC1f8Pi9GNaomw', '2007-07-23 13:34:51', '2009-08-10 06:05:26'),
(93, 'Dovie', 'Ernser', 'White Inc', 'ykihn@hotmail.com', '$2y$10$YEB3MpkhKNhr6wnD9kfQoeU4CGT2UfZ7gAaEzb6n12hGtgYkvLv3C', 'swaniawski.com', 'MIEdhlc4ULxJvJ2cDDDKF3FicgdLKdOI', '1991-03-12 20:49:08', '2015-06-06 00:11:09'),
(94, 'Madisyn', 'Wolff', 'Littel, Nolan and Boehm', 'lila.gibson@gmail.com', '$2y$10$L5kwTpL6VaUU4TFNvDOjbeMCOFOc7ndoi/I9bIOMyjPyvs/b5cG2m', 'klocko.org', 'xMMxUk0XduSzGbiS2AlvziPHm2fYBjrX', '1978-09-03 23:03:29', '1972-05-27 09:29:28'),
(95, 'Melissa', 'Mertz', 'Schmidt-Ortiz', 'pmurray@hotmail.com', '$2y$10$BWdjeyMbV7I0geNAeX39UeyFFG83JGtP5HtV.9EQ1b4D8kUbUElGa', 'marvin.com', 'ToUT1b7EGa6umkTML8BI30FbNl8ZPfow', '2016-01-06 19:58:58', '2006-10-17 08:40:26'),
(96, 'Vicente', 'Larson', 'Hamill Group', 'prau@armstrong.com', '$2y$10$DJI0xmA7BdiEwFyC/hLlA.8BSg87KdwebVdvK.jVH9Zb1Z9sALNoK', 'mcclure.com', 'ToobZKZujR5GNHkFjZqbKL2fZyHRfMwA', '1986-07-04 06:54:29', '1996-12-28 23:00:51'),
(97, 'Onie', 'Jacobson', 'Cronin, Boyle and Rice', 'jayne.kuphal@hotmail.com', '$2y$10$DIAsSY6Cwqvn.1iCJT91u.B7K0U2ymwRTdfhLa2C4ztJ8fGU/5Qky', 'carroll.org', 'tMDBK4gMquQMTdtwo2gU7iMPcXQDCMfn', '1996-10-26 23:46:53', '2009-08-26 13:32:05'),
(98, 'Ruthe', 'Hegmann', 'Herman, Jast and Lynch', 'citlalli09@terry.net', '$2y$10$exfHr97cF8a5LdjyhLYif.WVBWQWVP64S2riaavQuURcnVPaf676W', 'beatty.info', 'uknJmRTbCFsgTvpZmgr8Pm5EPCMh5WY4', '1998-06-19 16:37:43', '1976-05-06 13:02:21'),
(99, 'Berta', 'Senger', 'Block, Bashirian and Stoltenberg', 'bernier.grayce@abshire.com', '$2y$10$XQRHcgDcGp66S3YrC3gQ5uNiYlql8fHI4PxsHuhFDU0HweCgfr3Oa', 'murazik.com', 'wg3GWdap7TAYQ6QVuype6haWcRvXPeoa', '1996-09-09 08:51:47', '1973-07-27 06:27:07'),
(100, 'Axel', 'O\'Hara', 'Bahringer, Sanford and Gutmann', 'ubaldo83@gmail.com', '$2y$10$FOa9t6SrWOtQjieAFdNQbuO46iEdkwPCV5RJR4R0VKhopMFJCRdyC', 'feeney.com', 'OVnV1aZjA65xL1H0aU9xBNss98LMv0xV', '2003-05-10 04:34:51', '1988-04-14 07:55:41'),
(101, 'Dawson', 'Ledner', 'Emmerich-Adams', 'deion30@paucek.com', '$2y$10$e9XJjAhMxiTxpZ7hOdTaHeoL5V02iwvEPFErKhtXhIMGwkOmWR0HO', 'murphy.com', 'U9QJOSxJsZzJTCjmdWVDBAQgzBFipPNZ', '1990-03-29 18:45:47', '1973-06-16 15:07:15'),
(102, 'Danial', 'Corkery', 'Hand, Reichert and Nicolas', 'jordon.abbott@kuhn.biz', '$2y$10$3Oedffwx3Bmo8g3RDNlDruJ6SFoO/aeE9OLCdb4oBQJIqs0vQeyUS', 'jacobson.com', 'dHGQvP2AiXS3IQYUOFn7JoZVNkJkMMPw', '1978-05-23 05:50:28', '2019-11-14 04:37:53'),
(103, 'Eva', 'DuBuque', 'Botsford PLC', 'mgerhold@gmail.com', '$2y$10$VbysfGhi2XLFlZpdxHeMAu3N/8BKvt0SQDKtE0/8IBQ0nq7zxnmAa', 'renner.com', 'oxA5DddsfPdjHo6gcud0Zmggwl4HTIbr', '1995-05-16 00:11:51', '1994-07-18 16:46:31'),
(104, 'Laurence', 'Luettgen', 'Hand and Sons', 'gtrantow@pfeffer.biz', '$2y$10$SL64Qjiojzj2yZHLY6.9I.fqyEWdXPzuWD6iJQvCt6CgWq7AMdIVa', 'russel.org', '0V9ERZe3fMmmOyKXbGh3wRiFhcUjQqcl', '1976-09-14 03:51:22', '2013-01-17 19:43:08'),
(105, 'Delfina', 'Okuneva', 'Erdman-Mertz', 'zritchie@moore.com', '$2y$10$VVWNVPju691YQ.HK54R8R.Nrl2hzDij5.4RHvp2P25KHOwrW8HAdG', 'leuschke.info', 'VvHfPOwDTLv4eBp9XLEQUqkNfc5s4ffe', '2000-08-27 21:12:37', '2018-01-23 09:48:45'),
(106, 'Kevon', 'Langosh', 'Shanahan LLC', 'foster.aufderhar@hotmail.com', '$2y$10$SNcMXL2/bquoBAKVjITBZ.w5L3ZBzFvh/Rv/BK7Yw8QTUgFOlDYZ.', 'mayer.biz', 'tQ58nETF8wrc6r80o75xfaqhVb9cB13U', '1977-11-20 14:02:36', '2020-07-28 01:32:45'),
(107, 'Susana', 'Gorczany', 'Pfannerstill Ltd', 'emmerich.georgiana@torp.net', '$2y$10$7j8vd1N7cZSHvO5uidxLLezq66ldSsROV3gOgRLqs1ee3951fhAMm', 'hintz.org', '591ZLlxCSHMKNrZgPpIQILdpxwtbzucA', '2005-12-26 06:46:17', '1995-11-18 13:11:00'),
(108, 'Melissa', 'Rogahn', 'Schroeder, McGlynn and Orn', 'hermann.oleta@hotmail.com', '$2y$10$.GiJcCov.GzPQVUIucR74OmCtUcK59dP.7yIAsV0v7w3a9VOC0maK', 'bogisich.info', 'SOxFXNbPPqf5NzzEbvQE0sUWHp7BT2lT', '2013-09-19 01:18:34', '2011-04-03 22:58:39'),
(109, 'Dashawn', 'Langworth', 'Corkery LLC', 'achristiansen@hotmail.com', '$2y$10$y7hfQOXt9Y7AQGfO9dtR1.Hhd2D03f.q30iaOQXOtP0qi2m7g2AGu', 'rolfson.com', 'QpStqyd4T3zCqnxvMIi5vVtotu8oseV1', '1999-12-29 22:26:12', '2003-11-19 12:46:12'),
(110, 'Margarett', 'Pfannerstill', 'Hartmann Ltd', 'dleannon@koss.info', '$2y$10$PanSJmFb.fK6NEnapYCUgOtbH.rdir202qy.y./j5.vGJTiycyE2O', 'hudson.com', 'r3ac0aVVHcD7obfiuhzai1iTsn5B7Prt', '1985-07-19 11:12:27', '1973-08-12 00:29:39'),
(111, 'Leatha', 'Klein', 'O\'Hara Ltd', 'sporer.lura@kiehn.net', '$2y$10$cWZSOU3jYQZf.njBLmhHAubiSx0aXBsv72mz1wgZAUCh5fqLgvMba', 'stroman.net', 'WhV7oTFZTkBFTs0rRJsapcE5FMIb5AW1', '1986-04-10 06:56:11', '2006-11-02 18:14:07'),
(112, 'Rosalind', 'Kuhn', 'Bayer Inc', 'funk.brooklyn@prohaska.com', '$2y$10$A0U7Z3U0ACkrWC2VyNc3UuGWCHndWmaBOVw7wXu4f7a8bdad.QMAW', 'littel.org', 'sZI0eT5RD2slDbRSkUA88gZfVtihgtFS', '1976-05-09 16:47:32', '1990-10-06 03:12:00'),
(113, 'Talia', 'VonRueden', 'Abernathy and Sons', 'hermann.kaci@gmail.com', '$2y$10$2EA1GkOb/h/F2Re2878yr.AG4u.JmjZJ8tB9SiaJ6JNjo/9ynZdPO', 'zieme.com', 'x0ldxCmMN6lwQtNIAJvQPSuVVbyTaHb6', '2002-04-23 20:03:55', '2020-09-21 13:57:19'),
(114, 'Gavin', 'Brakus', 'Nienow-Wuckert', 'dgreen@hagenes.com', '$2y$10$1Z7QXaLy8wLkqjtykbIOee9cBcFPsHZ1URGl/Il5AXDuseIuqdVU.', 'mosciski.net', 'dJmuxKbFKQGIfOOtzrPGtHxRkIahLAma', '1980-12-02 04:07:51', '1995-02-11 01:14:41'),
(115, 'Hubert', 'Upton', 'Schaden-Orn', 'mark.ledner@wintheiser.com', '$2y$10$3F2fz20bW6eH9l9GH1cJiOCun1F21NHOqBu9aLKUH3//.Kj7CqHsG', 'bins.com', 'drxxLO68MlHXJ0ZjUFujYrHi0uof35ym', '1992-08-26 13:26:00', '1985-01-13 10:41:35'),
(116, 'Abdullah', 'Runolfsdottir', 'Welch, Thiel and Torphy', 'josianne48@gmail.com', '$2y$10$6SLtC60ZNCc5Hum1v2XtZ.YHwF8jqdPoI6BG8hamuJreorbmgu14C', 'smitham.org', 'pRJHohhoaJIcHr7t8RkmZ5abAuRXAUiU', '2012-02-26 21:15:05', '1980-07-18 00:27:31'),
(117, 'Lulu', 'Buckridge', 'Ullrich-Becker', 'nettie65@hotmail.com', '$2y$10$k/0XSFj51HPpPd546MYABuyWVhETz3own3tAO1srmtSyqf6EuSJTG', 'will.biz', 'aIVJdOpojDEHwsMNyoUTqEDhjVblDSn5', '2007-02-19 09:01:42', '2006-08-13 12:09:09'),
(118, 'Carmella', 'Breitenberg', 'Bogan and Sons', 'caroline.renner@schoen.com', '$2y$10$pg7WIyuiU/DPcMFfR/EV8eV0yASrmpqZ2he14Ha7BEvEKC/uCDkyi', 'oconner.com', 'Rnw2WgOixorBsDpj7kLJT4LBtfdII9ze', '2018-10-05 15:21:14', '2006-06-14 05:59:44'),
(119, 'Ethyl', 'Daniel', 'Franecki-Hartmann', 'ewiegand@cassin.org', '$2y$10$5F5PPyKfs3M2PTXYJ9bdneCD8lY5PFKKQ4FCLVxTlPK03ENb3WQSG', 'gerhold.org', 'mddvYOes8Ua9AmEWjKBPtwP5ZoVRL2bp', '1994-04-23 21:38:02', '1973-11-24 04:58:24'),
(120, 'Florencio', 'Mante', 'Dietrich and Sons', 'phermiston@hotmail.com', '$2y$10$4Fpd1tjYJaoMobzhq8QPMuteNMhtc1ZQE4vlkV1phu0NA3JjT2vDy', 'botsford.net', 'PmXTTqE9SNnvPeIEcibO9hALLq698dr0', '1971-03-16 15:07:11', '1999-12-09 09:07:31'),
(121, 'Rosanna', 'Smitham', 'Tillman, Kerluke and Grimes', 'medhurst.demond@simonis.com', '$2y$10$LJHXK9rBAe1xC6vz8JTv4uuSExMIqJI4abC14qne/9224Smt/Ke9u', 'heller.com', '9e9OC9wNIOgqiCoBA27lRg1Jpwh6mwx4', '2004-11-15 11:06:38', '2011-04-16 09:41:47'),
(122, 'Lou', 'Will', 'Schroeder Group', 'eugene.wiza@gmail.com', '$2y$10$oTP3v6INe9pyB1Dhfjmaxe3MaVvKAZX6Zhn70P.nb1GxRvRFSXlJC', 'rowe.com', 'PSvXPmy5mQapDCZ1ogOQWIlLkMArJj9j', '1982-09-13 00:57:14', '2018-01-13 21:09:18'),
(123, 'Joan', 'Walker', 'Ondricka LLC', 'rolfson.ernestina@hotmail.com', '$2y$10$2mjclxiuw/NWhZp.hX8v2ODb0naIKrcTI1mg2VGoaVAAnl7NtGS12', 'wunsch.com', 'mnLybono3vlhTLIyS03EkbgpWfr5JWsr', '1978-12-22 22:35:39', '2014-07-08 21:03:52'),
(124, 'Ronny', 'Shanahan', 'Moore and Sons', 'willms.ella@wolf.com', '$2y$10$aU4awa.4/f/gHPeLGS4xNOKsYf2tITG0VaCk0yGyhKH.c/w/qpJw.', 'heathcote.biz', 'tlc9CNkhIsDvTQLjjp7IZ5VHK378bptf', '1986-09-18 06:35:44', '2008-12-26 04:47:10'),
(125, 'Matteo', 'King', 'Dickinson, Becker and Botsford', 'qkihn@hotmail.com', '$2y$10$4raVPjkfu3MzkrBZNrfBP.avmCHJj1rhkZshTujGBLE/GQ8bkFxjK', 'gorczany.com', 'hTA9QX7XrGyAZFRKRkx96yLnTZ60f5bE', '1982-07-24 00:24:48', '2021-02-10 13:51:50'),
(126, 'Ryan', 'Robel', 'Nader PLC', 'kaley91@torphy.com', '$2y$10$.MK3yEMzdSVAbm9Le28vseV4LrQplEILsiGy156e0hfYm9BR9onpS', 'schneider.com', '8PLsKkHJIGzFxwNGtlBLlveuEuiMRJ00', '2019-07-20 18:29:53', '2021-05-26 03:55:45'),
(127, 'Morgan', 'Sawayn', 'Grimes, Kilback and Zieme', 'amos53@yahoo.com', '$2y$10$QCG0T1Nf3Jp/p4UVfeT8i.09k3E8KHWA5MYEUxhWiwNFVkrHq.NL2', 'nitzsche.com', 'm2jI01uYxsbB8XwPiE59SYzkWS5SOpZY', '1992-04-13 09:24:34', '1992-01-15 04:32:07'),
(128, 'Deneme', 'Deneme', 'Deneme', 'Deneme@deneme.com', '$2y$10$geUBSyVQqIVl5tdwvTuFvu6PuMlZTpS1NMrckrabcpeXqUHOeILkq', 'deneme.com', 'RCg2r4wlxLlaQBOWEYfCufmuhZoCGNOQ', '2021-08-18 11:31:01', '2021-08-18 11:31:01'),
(129, 'Deneme', 'Deneme', 'Deneme', 'Den1eme@deneme.com', '$2y$10$HbG7l12QcmS0aGu8gGsL5O8D9njfSjhvwEyAmzQoqV454ahu5JeSq', 'deneme.com', 'B4jBKYPvvcQ6jTw5ZvWgdG5akA1PrIku', '2021-08-18 11:44:51', '2021-08-18 11:44:51');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `CompanyPackages`
--

CREATE TABLE `CompanyPackages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `end_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `CompanyPackages`
--

INSERT INTO `CompanyPackages` (`id`, `company_id`, `package_id`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(2, 2, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(3, 3, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(4, 4, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(5, 5, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(6, 6, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(7, 7, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(8, 8, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(9, 9, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(10, 10, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(11, 11, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(12, 12, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(13, 13, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(14, 14, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(15, 15, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(16, 16, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(17, 17, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(18, 18, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(19, 19, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(20, 20, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(21, 21, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(22, 22, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(23, 23, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(24, 24, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(25, 25, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(26, 26, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(27, 27, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(28, 28, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(29, 29, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(30, 30, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(31, 31, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(32, 32, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(33, 33, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(34, 34, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(35, 35, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(36, 36, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(37, 37, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(38, 38, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(39, 39, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(40, 40, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(41, 41, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(42, 42, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(43, 43, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(44, 44, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(45, 45, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(46, 46, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(47, 47, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(48, 48, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(49, 49, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(50, 50, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(51, 51, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(52, 52, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(53, 53, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(54, 54, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(55, 55, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(56, 56, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(57, 57, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(58, 58, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(59, 59, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(60, 60, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(61, 61, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(62, 62, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(63, 63, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(64, 64, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(65, 65, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(66, 66, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(67, 67, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(68, 68, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(69, 69, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(70, 70, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(71, 71, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(72, 72, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(73, 73, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(74, 74, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(75, 75, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(76, 76, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(77, 77, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(78, 78, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(79, 79, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(80, 80, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(81, 81, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(82, 82, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(83, 83, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(84, 84, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(85, 85, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(86, 86, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(87, 87, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(88, 88, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(89, 89, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(90, 90, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(91, 91, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(92, 92, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(93, 93, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(94, 94, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(95, 95, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(96, 96, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(97, 97, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(98, 98, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(99, 99, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(100, 100, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(101, 101, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(102, 102, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(103, 103, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(104, 104, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(105, 105, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(106, 106, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(107, 107, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(108, 108, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(109, 109, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(110, 110, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(111, 111, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(112, 112, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(113, 113, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(114, 114, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(115, 115, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(116, 116, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(117, 117, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(118, 118, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(119, 119, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(120, 120, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(121, 121, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(122, 122, 1, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(123, 123, 1, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(124, 124, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(125, 125, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(126, 126, 2, '2022-08-18 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(127, 127, 2, '2021-09-17 11:30:51', '2021-08-18 11:30:51', '2021-08-18 11:30:51'),
(128, 128, 2, '2022-08-18 11:45:48', '2021-08-18 11:31:03', '2021-08-18 11:45:48'),
(131, 129, 2, '2022-08-18 11:45:54', '2021-08-18 11:45:54', '2021-08-18 11:45:54');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `CompanyPayments`
--

CREATE TABLE `CompanyPayments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_paid` int(11) NOT NULL,
  `is_tried` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2021_08_18_122036_companies', 1),
(3, '2021_08_18_123456_packages', 1),
(4, '2021_08_18_123647_company_packages', 1),
(5, '2021_08_18_123815_company_payments', 1);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `Packages`
--

CREATE TABLE `Packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `period` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `Packages`
--

INSERT INTO `Packages` (`id`, `name`, `period`) VALUES
(1, 'Monthly', '30'),
(2, 'Yearly', '365');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `Companies`
--
ALTER TABLE `Companies`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companies_token_unique` (`token`);

--
-- Tablo için indeksler `CompanyPackages`
--
ALTER TABLE `CompanyPackages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `companypackages_company_id_unique` (`company_id`);

--
-- Tablo için indeksler `CompanyPayments`
--
ALTER TABLE `CompanyPayments`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `Packages`
--
ALTER TABLE `Packages`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `Companies`
--
ALTER TABLE `Companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- Tablo için AUTO_INCREMENT değeri `CompanyPackages`
--
ALTER TABLE `CompanyPackages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- Tablo için AUTO_INCREMENT değeri `CompanyPayments`
--
ALTER TABLE `CompanyPayments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Tablo için AUTO_INCREMENT değeri `Packages`
--
ALTER TABLE `Packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Tablo için AUTO_INCREMENT değeri `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
