# Kobisi API documentation

Ahmet Hakan Çelik

Postman Collection -> [kobisi.postman_collection.json](https://gitlab.com/ahmethakan/kobisi/-/blob/master/kobisi.postman_collection.json)

Kobisi.sql -> [kobisi.sql](https://gitlab.com/ahmethakan/kobisi/-/blob/master/kobisi.sql)


**Worker not included**

# REST API

The REST API to the example app is described below.

## Create a new company

### Request

`POST /api/register`

    http://127.0.0.1:8000/api/register

    {
        "name" : "Deneme",
        "lastname" : "Deneme",
        "company_name" : "Deneme",
        "email" : "Deneme@deneme.com",
        "password": "Deneme",
        "site_url": "deneme.com"
    }

### Response

    Status: 201 Created

    {
        "message": "Company successfully registered.",
        "id": 129,
        "token": "B4jBKYPvvcQ6jTw5ZvWgdG5akA1PrIku"
    }

## Register New Company Package

### Request

`POST /api/companypackage`

    http://127.0.0.1:8000/api/companypackage

    {
        "company_id" : "128",
        "package_id": "2"
    }

### Response

    Status: 201 OK

    {
        "message": "Company package successfully registered.",
        "start date": "2021-08-18T14:45:54.259816Z",
        "end date": "2022-08-18T14:45:54.257545Z",
        "package": {
            "id": 2,
            "name": "Yearly",
            "period": "365"
        }
    }

## Get Company Data

### Request

`GET /api/companydata`

    http://127.0.0.1:8000/api/companydata?token=MImEd6HV16LX83v2pTUqqRjErfPW8KaE

### Response

    Status: 201 Created

    {
        "Company": {
            "id": 1,
            "name": "Ransom",
            "lastname": "Kilback",
            "email": "pschowalter@roob.com",
            "company_name": "Deckow, Kassulke and Boyle",
            "site_url": "lesch.com",
            "token": "jVyJxcFOYdqplQheHcGwwC5vHI785tFl"
        },
        "Package": {
            "id": 2,
            "name": "Yearly",
            "period": "365"
        },
        "Paket Kayıt Tarihi": "2021-08-18T14:30:51",
        "Paket Bitiş Tarihi": "2021-09-17 14:30:51"
    }
