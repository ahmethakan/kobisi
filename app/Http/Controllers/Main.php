<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

use App\Models\Company;
use App\Models\Package;
use App\Models\CompanyPackage;


class Main extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Company = Company::where('token', $request->token)->first();

        if( $Company )
        {
            $CompanyPackage = CompanyPackage::where('company_id', $Company->id)->first();

            if( $CompanyPackage )
            {
                $Package = Package::find($CompanyPackage->package_id);

                if( $Package )
                {
                    return response()->json([
                        'Company' => array(
                            'id' => $Company->id,
                            'name' => $Company->name,
                            'lastname' => $Company->lastname,
                            'email' => $Company->email,
                            'company_name' => $Company->company_name,
                            'site_url' => $Company->site_url,
                            'token' => $Company->token,

                        ),
                        'Package' => $Package,
                        'Paket Kayıt Tarihi' => $CompanyPackage->created_at,
                        'Paket Bitiş Tarihi' => $CompanyPackage->end_date,
                    ], 201 );

                    return $data;
                }
            }

            return response()->json([
                'message' => 'Company does not have a package.'
            ], 404 );
        }

        return response()->json([
            'message' => 'Company is not found.'
        ], 404 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $isRegistered = Company::Where('email', $request->email)->first();

        if( !$isRegistered )
        {
            $Company = new Company;

            $Company->name = $request->name;
            $Company->lastname = $request->lastname;
            $Company->company_name = $request->company_name;
            $Company->email = $request->email;
            $Company->password = bcrypt($request->password);
            $Company->site_url = $request->site_url;
            $Company->token = Str::random(32);

            $Company->save();

            if( $Company )
            {
                return response()->json([
                    'message' => 'Company successfully registered.',
                    'id' => $Company->id,
                    'token' => $Company->token
                ], 201 );
            }
        }

        return response()->json([
            'message' => 'Company is already registered.'
        ], 404 );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Company = Company::where('id', $request->company_id)->first();

        if( $Company )
        {
            $Package = Package::where('id', $request->package_id)->first();

            if( $Package )
            {
                if( $Package->id == 1 )
                {
                    $end_date = Carbon::now()->addDays(30);
                }
                else
                {
                    $end_date = Carbon::now()->addDays(365);
                }

                $isPackaged = CompanyPackage::where('company_id', $Company->id)->first();

                if( !$isPackaged )
                {
                    $CompanyPackage = new CompanyPackage;

                    $CompanyPackage->company_id = $Company->id;
                    $CompanyPackage->package_id = $Package->id;
                    $CompanyPackage->end_date = $end_date;

                    $CompanyPackage->save();

                    if( $CompanyPackage )
                    {
                        return response()->json([
                            'message' => 'Company package successfully registered.',
                            'start date' => now(),
                            'end date' => $CompanyPackage->end_date,
                            'package' => $Package
                        ], 201 );
                    }
                }
                else
                {
                    $CompanyPackage = CompanyPackage::where('company_id', $Company->id)->first();

                    $CompanyPackage->package_id = $Package->id;
                    $CompanyPackage->end_date = $end_date;

                    $CompanyPackage->save();

                    return response()->json([
                        'message' => 'Company package successfully upgraded.',
                        'start date' => now(),
                        'end date' => $CompanyPackage->end_date,
                        'package' => $Package
                    ], 200 );

                }

            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
