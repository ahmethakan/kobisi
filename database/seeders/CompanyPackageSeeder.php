<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Str;


class CompanyPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=1; $i<128; $i++)
        {
            $package = rand(1,2);

            if( $package == 1 )
            {
                $end_date = Carbon::now()->addDays(30);
            }
            else
            {
                $end_date = Carbon::now()->addDays(365);
            }

            DB::table('CompanyPackages')->insert([
                'company_id' => $i,
                'package_id' => rand(1,2),
                'end_date' => $end_date,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
