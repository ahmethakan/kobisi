<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        DB::table('Packages')->insert([
            'name' => 'Monthly',
            'period' => 30,
        ]);

        DB::table('Packages')->insert([
            'name' => 'Yearly',
            'period' => 365,
        ]);
    }
}