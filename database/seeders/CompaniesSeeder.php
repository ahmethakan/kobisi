<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=1; $i<128; $i++)
        {
            DB::table('Companies')->insert([
                'name' => $faker->firstName(),
                'lastname' => $faker->lastName(),
                'company_name' => $faker->company(),
                'email' => $faker->email(),
                'password' => bcrypt('123456'),
                'site_url' => $faker->domainName(),
                'token' => Str::random(32),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime()
            ]);
        }
    }
}
